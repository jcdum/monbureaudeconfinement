package com.thuriercyril.bureauconfinement.sharedfilesystem;

import java.io.Serializable;
import java.util.function.Predicate;

import com.hazelcast.map.EventJournalMapEvent;

public class AcceptsAll<T> implements Predicate<EventJournalMapEvent<String, T>>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5018281329064566581L;

	@Override
	public boolean test(EventJournalMapEvent<String, T> t) {
		return true;
	}

}
