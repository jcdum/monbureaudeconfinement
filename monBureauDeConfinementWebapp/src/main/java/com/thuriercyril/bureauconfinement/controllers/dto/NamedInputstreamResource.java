package com.thuriercyril.bureauconfinement.controllers.dto;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.core.io.AbstractResource;

import com.thuriercyril.bureauconfinement.sharedfilesystem.LocalFilesDescriptor;

public class NamedInputstreamResource extends AbstractResource{

	private InputStream inpustream;
	private String name;
	private long length;

	public NamedInputstreamResource(byte[] data, LocalFilesDescriptor fileMetadata) {
		inpustream = new ByteArrayInputStream(data);
		name = fileMetadata.getName();
		length = fileMetadata.getSize();
	}

	@Override
	public String getDescription() {
		return name;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return inpustream;
	}

	@Override
	public long contentLength() throws IOException {
		return length;
	}

	@Override
	public String getFilename() {
		return name;
	}


}
