package com.thuriercyril.bureauconfinement.controllers.dto;

import lombok.Getter;
import lombok.Setter;

public class MessageDto {
	
	@Getter
	@Setter
	private String message;	
	

}
