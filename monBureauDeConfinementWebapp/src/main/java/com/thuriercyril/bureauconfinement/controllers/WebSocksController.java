package com.thuriercyril.bureauconfinement.controllers;

import org.springframework.context.annotation.DependsOn;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@DependsOn("localFileSystemManager")
@Controller
public class WebSocksController {
	
	@MessageMapping("/ping")
	@SendTo("/topic/reply")
	public String someoneJoined(String message) {
		return "pong";
	}
	
}

