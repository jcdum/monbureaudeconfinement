package com.thuriercyril.bureauconfinement.controllers.dto;

import lombok.Getter;
import lombok.Setter;

public class Redirection {
	
	@Getter
	@Setter
	private String redirection;

}
