package com.thuriercyril.bureauconfinement.sharedfilesystem;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;

import com.hazelcast.jet.JetException;
import com.hazelcast.jet.pipeline.SourceBuilder;

public class SocketSource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8860175254580895333L;
	private transient ServerSocket socket;
	private transient Socket socketOfServer;
	private transient InputStream is;
	
	private int port;
	private int start;

	public SocketSource(int port) {
		this.port = port;
		this.start = 0;
	}


	public void init() throws IOException {
		this.socket = new ServerSocket(port);
		socketOfServer = socket.accept();
		is = new BufferedInputStream(socketOfServer.getInputStream());		
	}


	public void feed(SourceBuilder.SourceBuffer<byte[]> buf) {
		try {
			if(is == null) {
				init();
			}
			
			if(socketOfServer.isClosed()) {
				buf.close();
				return;
			}
			
			long skipped = 0;
			while (skipped < start) {
				skipped += is.skip(start);
			}

			int read = -1;
			
			byte[] buffer = new byte[2048];

			while ((read = is.read(buffer)) > 0) {

				byte[] data = new byte[read];
				System.arraycopy(buffer, 0, data, 0, read);
				buf.add(data);
			}
			is.close();
			is = null;
			buf.close();
			socket.close();
		} catch (Exception e) {
			if(is != null) {
				try {
					buf.close();
					is.close();
					is = null;					
					socket.close();
				}catch (Exception ee) {
					ee.printStackTrace();
				}
			}
		}
	}


	public void close() {
		if(is != null) {
			try {
				is.close();
				is = null;	
				socket.close();
			} catch (IOException e) {
				if(is != null) {
					try {
						is.close();
						is = null;					
						socket.close();
					}catch (Exception ee) {
						ee.printStackTrace();
					}
				}
			}
		}
	}

}
