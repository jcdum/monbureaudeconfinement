package com.thuriercyril.bureauconfinement;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.event.EventListener;
import org.springframework.web.context.WebApplicationContext;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication(exclude = HazelcastAutoConfiguration.class)
@Slf4j
@NoArgsConstructor
public class WebApp {
	
	@Value("${home}")
	private String home;
	

	public static void main(String[] args) throws Exception {
		SpringApplication.run(WebApp.class, args);
	}

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public MaSession session() {
		return new MaSession(log);
	}

	@EventListener({ ApplicationReadyEvent.class })
	void applicationReadyEvent() {
		System.out.println("Application started ... launching browser now");
		browse(home);
	}

	public static void browse(String url) {
		System.setProperty("java.awt.headless", "false");
		try {
			if (Desktop.isDesktopSupported()) {
				Desktop desktop = Desktop.getDesktop();

				desktop.browse(new URI(url));

			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

}
