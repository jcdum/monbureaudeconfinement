package com.thuriercyril.bureauconfinement.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {
	
	@Autowired
	private CommonRestController commonRestController;



	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String index(Model model) {
		commonRestController.populate(model);
		return "index";
	}

	@RequestMapping(value = { "/main" }, method = RequestMethod.GET)
	public String enroll(Model model) {
		
		if (commonRestController.checkSession()) {
			commonRestController.populate(model);
			return "main :: main";
		} else {
			return "register :: register";
		}
	}


	

}
