package com.thuriercyril.bureauconfinement.sharedfilesystem;

import java.io.Serializable;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.hazelcast.function.ConsumerEx;
import com.hazelcast.function.FunctionEx;
import com.hazelcast.function.PredicateEx;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.Job;
import com.hazelcast.jet.Observable;
import com.hazelcast.jet.Util;
import com.hazelcast.jet.function.Observer;
import com.hazelcast.jet.impl.util.ImdgUtil;
import com.hazelcast.jet.pipeline.BatchSource;
import com.hazelcast.jet.pipeline.JournalInitialPosition;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sink;
import com.hazelcast.jet.pipeline.SinkBuilder;
import com.hazelcast.jet.pipeline.Sinks;
import com.hazelcast.jet.pipeline.SourceBuilder;
import com.hazelcast.jet.pipeline.Sources;
import com.hazelcast.map.EventJournalMapEvent;
import com.thuriercyril.bureauconfinement.MaSession;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SharedFileSystem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6110157452976818393L;

	public static final String SOURCES = "shared-sources";

	public static final String LISTENERS = "listeners";

	public static SharedFileSystem INSTANCE;

	@Autowired
	private transient JetInstance jet;

	@Value("${dossier-partage}")
	private String location;

	private String address;

	private transient Observable<Entry<String, FileRequest>> observable;

	@Value("${transfer-socket}")
	private int serverSocketPort;

	public SharedFileSystem() {
		SharedFileSystem.INSTANCE = this;
	}

	public void getContent(LocalFilesDescriptor fileMetadata, long start, long rangeLength, MaSession session) {

		Sink<byte[]> sink = session.getSink(jet);

		String key = "file-source#" + fileMetadata.getDisplayPath() + "@" + fileMetadata.getOwner().getUser();

		log.info("Request file {}", key);

		Pipeline p = Pipeline.create();

		BatchSource<byte[]> source = SourceBuilder.<SocketSource>batch(key, c -> new SocketSource(serverSocketPort))
				.destroyFn(s -> s.close()).<byte[]>fillBufferFn((s, consumer) -> s.feed(consumer)).build();

		FileRequest request = new FileRequest();
		request.setFile(fileMetadata);
		request.setStatus(Status.REQUEST);
		request.setToken(UUID.randomUUID());
		request.setHost(address);
		request.setPort(serverSocketPort);
		jet.getHazelcastInstance().getMap(LISTENERS).put(key, request);

		p.readFrom(source).writeTo(sink);

		Job job = jet.newJob(p);
		session.withJob(job);
	}

	public BatchSource<byte[]> loadFile(LocalFilesDescriptor fileMetadata, long start, long rangeLength) {

		String key = "file-source#" + fileMetadata.getDisplayPath() + "@" + fileMetadata.getOwner().getUser();

		if (rangeLength < 0) {
			rangeLength = fileMetadata.getSize();
		}
		try {

			return SourceBuilder.<LocalFilesDescriptor>batch(key, ctx -> fileMetadata)
					.fillBufferFn(Streamer.source(fileMetadata)).build();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public List<LocalFilesDescriptor> listFiles() {
		return jet.getHazelcastInstance().getMap("shared-FS").values().stream().map(o -> ((LocalFilesDescriptor) o))
				.sorted().collect(Collectors.toList());

	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setup() {

		// Share
		Pipeline p1 = Pipeline.create();
		observable = jet.newObservable();

		Predicate<EventJournalMapEvent<String, FileRequest>> pred = new AcceptsAll<FileRequest>();
		PredicateEx<EventJournalMapEvent<String, FileRequest>> predEx = ImdgUtil
				.<EventJournalMapEvent<String, FileRequest>>wrapImdgPredicate(pred);
		FunctionEx<EventJournalMapEvent<String, FileRequest>, Entry<String, FileRequest>> fun = Util
				.<String, FileRequest>mapEventToEntry();

		p1.readFrom(Sources.mapJournal(LISTENERS, JournalInitialPosition.START_FROM_OLDEST, fun, predEx))
				.withoutTimestamps().writeTo(Sinks.observable(observable));

		observable.addObserver(Observer.of(new ConsumerEx<Entry<String, FileRequest>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -5772115353998451460L;

			@Override
			public void acceptEx(Entry<String, FileRequest> t) throws Exception {

				if (t.getValue().getFile().getOwner().getAddress().equals(address)
						&& t.getValue().getStatus().equals(Status.REQUEST)) {

					String host = t.getValue().getHost();
					int port = t.getValue().getPort();
					String name = t.getKey();

					log.info("asked {}", name);

					LocalFilesDescriptor file = t.getValue().getFile();
					BatchSource<byte[]> source = loadFile(file, 0, -1);

					Pipeline p = Pipeline.create();

					Sink<byte[]> sink = SinkBuilder
							.<SocketSink>sinkBuilder("key", c -> new SocketSink(host, port, name))
							.destroyFn(s -> s.close()).<byte[]>receiveFn((s, c) -> s.eat(c)).build();

					p.readFrom(source).rebalance().writeTo(sink);
					t.getValue().setStatus(Status.SOURCED);
					jet.getHazelcastInstance().getMap(LISTENERS).put(t.getKey(), t.getValue());

					jet.newJob(p);

				}

			}
		},

				e -> {
					e.printStackTrace();
				}, () -> {
				}));

		jet.newJob(p1);

	}

}
