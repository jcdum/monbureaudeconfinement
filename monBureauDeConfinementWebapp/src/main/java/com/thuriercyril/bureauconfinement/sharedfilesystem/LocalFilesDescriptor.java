package com.thuriercyril.bureauconfinement.sharedfilesystem;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.hazelcast.jet.pipeline.BatchSource;
import com.hazelcast.jet.pipeline.Sink;
import com.hazelcast.jet.pipeline.Sinks;
import com.thuriercyril.bureauconfinement.user.CompleteUser;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class LocalFilesDescriptor implements Serializable, Comparable<LocalFilesDescriptor> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3300477704901880768L;

	@Getter
	@Setter
	private CompleteUser owner;

	@Getter
	@Setter
	private String path;

	@Getter
	@Setter
	private String displayPath;

	@Getter
	@Setter
	private LocalDateTime date;

	@Getter
	@Setter
	private long size;
	
	@Getter
	@Setter
	private String key;
	
	@Getter
	@Setter
	private String name;
	
	@Getter
	@Setter
	private String parent;
	
	

	@Override
	public int compareTo(LocalFilesDescriptor o) {
		int first = owner.getUser().compareTo(o.owner.getUser());
		return first == 0 ? displayPath.compareTo(o.displayPath) : first;
	}

}
