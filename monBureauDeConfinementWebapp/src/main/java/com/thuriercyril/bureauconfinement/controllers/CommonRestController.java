package com.thuriercyril.bureauconfinement.controllers;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.servlet.http.HttpServletRequest;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourceRegion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRange;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.thuriercyril.bureauconfinement.MaSession;
import com.thuriercyril.bureauconfinement.controllers.dto.FileItemDto;
import com.thuriercyril.bureauconfinement.controllers.dto.MessageDto;
import com.thuriercyril.bureauconfinement.controllers.dto.NamedInputstreamResource;
import com.thuriercyril.bureauconfinement.controllers.dto.Redirection;
import com.thuriercyril.bureauconfinement.controllers.dto.UserDto;
import com.thuriercyril.bureauconfinement.sharedfilesystem.LocalFileSystemManager;
import com.thuriercyril.bureauconfinement.sharedfilesystem.LocalFilesDescriptor;
import com.thuriercyril.bureauconfinement.sharedfilesystem.SharedFileSystem;
import com.thuriercyril.bureauconfinement.tchat.TchatService;
import com.thuriercyril.bureauconfinement.user.CompleteUser;
import com.thuriercyril.bureauconfinement.user.UserService;

@RestController
public class CommonRestController {

	@Autowired
	private MaSession maSession;

	@Autowired
	private UserService userService;

	@Autowired
	private SharedFileSystem sharedFileSystem;

	@Autowired
	private TchatService tchatService;

	@Autowired
	private SimpMessageSendingOperations messagingTemplate;

	@Autowired
	private transient LocalFileSystemManager localFileSystemManager;
	
	@Value("${home}")
	private String home;

	private Subscription subscriptionFiles;

	private Subscription subscriptionUsers;

	private Subscription subscriptionTchat;

	@PostMapping("/register")
	@ResponseBody
	public Redirection submitUser(@RequestBody UserDto user) throws IOException {
		CompleteUser completeuser = userService.registerUser(user.getName());

		sharedFileSystem.setup();
		localFileSystemManager.setup(completeuser);
		localFileSystemManager.getObservable().subscribe(new Subscriber<String>() {

			@Override
			public void onSubscribe(Subscription s) {
				if (CommonRestController.this.subscriptionFiles != null) {
					CommonRestController.this.subscriptionFiles.cancel();
				}
				CommonRestController.this.subscriptionFiles = s;
				s.request(10);
			}

			@Override
			public void onNext(String t) {
				messagingTemplate.convertAndSend("/topic/update", t);
				CommonRestController.this.subscriptionFiles.request(1);
			}

			@Override
			public void onError(Throwable t) {
				t.printStackTrace();
			}

			@Override
			public void onComplete() {
			}
		});

		userService.setup();
		userService.getObservable().subscribe(new Subscriber<List<String>>() {

			@Override
			public void onSubscribe(Subscription s) {
				if (CommonRestController.this.subscriptionUsers != null) {
					CommonRestController.this.subscriptionUsers.cancel();
				}
				CommonRestController.this.subscriptionUsers = s;
				s.request(10);
			}

			@Override
			public void onNext(List<String> t) {
				messagingTemplate.convertAndSend("/topic/update", t.toArray(new String[t.size()]));
				CommonRestController.this.subscriptionUsers.request(1);

			}

			@Override
			public void onError(Throwable t) {

			}

			@Override
			public void onComplete() {

			}

		});

		localFileSystemManager.watch();

		tchatService.setup();
		tchatService.getObservable().subscribe(new Subscriber<List<String>>() {

			@Override
			public void onSubscribe(Subscription s) {
				if (CommonRestController.this.subscriptionTchat != null) {
					CommonRestController.this.subscriptionTchat.cancel();
				}
				CommonRestController.this.subscriptionTchat = s;
				s.request(10);
			}

			@Override
			public void onNext(List<String> t) {
				messagingTemplate.convertAndSend("/topic/update", t);
				CommonRestController.this.subscriptionTchat.request(1);
			}

			@Override
			public void onError(Throwable t) {
				t.printStackTrace();
			}

			@Override
			public void onComplete() {
			}

		});

		return new Redirection() {
			{
				setRedirection("main");
			}
		};
	}

	@PostMapping("/message")
	@ResponseBody
	public Redirection submitMessage(@RequestBody MessageDto message) throws IOException {

		tchatService.addMessage(maSession.getUser().getUser() + ": " + message.getMessage());

		return new Redirection() {
			{
				setRedirection("main");
			}
		};
	}

	@GetMapping("/logout")
	public Redirection logout(HttpServletRequest request) {

		localFileSystemManager.clear();
		userService.clear();
		request.getSession().invalidate();
		return new Redirection();
	}

	@GetMapping("/main/files")
	public Redirection getFiles(Model model, @RequestParam("page") Optional<Integer> page,
			@RequestParam("size") Optional<Integer> size) {

		int currentPage = page.orElse(1);
		int pageSize = size.orElse(5);

		Page<FileItemDto> filesPage = getFilesPage(currentPage, pageSize);
		model.addAttribute("filesPage", filesPage);

		int totalPages = filesPage.getTotalPages();
		if (totalPages > 0) {
			List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
			model.addAttribute("pageNumbers", pageNumbers);
		}

		maSession.addAttributes(model.asMap());

		return new Redirection() {
			{
				setRedirection("main");
			}
		};
	}

	@GetMapping(path = "/main/dl/{uuid}",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<?> downloadFile(@PathVariable("uuid") String uuid, @RequestHeader HttpHeaders headers)
			throws IOException {

		String filepath = (String) maSession.toId(uuid);

		LocalFilesDescriptor fileMetadata = localFileSystemManager.getFileDescriptor(filepath);

		long positions[] = getPositions(fileMetadata, headers);

		if (positions != null) {

			ResourceRegion region = resourceRegion(fileMetadata, positions);

			ResponseEntity<ResourceRegion> entity = ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
					.contentType(MediaType.APPLICATION_OCTET_STREAM)
					.header("Content-Disposition", "attachment; filename=" + fileMetadata.getName() + "")
					.contentLength(fileMetadata.getSize())					
					.header("Content-Length", Long.toString(fileMetadata.getSize())).body(region);

			long copied = positions[2];

			entity.getHeaders().replace("Content-Range",
					Arrays.asList("bytes " + positions[0] + "-" + (positions[0] + copied - 1) + "/" + positions[1]));

			return entity;
		} else {

			Resource resource = fullResource(fileMetadata);
			ResponseEntity<Resource> entity = ResponseEntity.status(HttpStatus.OK)
					.contentType(MediaType.APPLICATION_OCTET_STREAM)
					.header("Content-Disposition", "attachment; filename=" + fileMetadata.getName() + "")
					.contentLength(fileMetadata.getSize())
					.body(resource);
					
			return entity;

		}

	}

	long[] getPositions(LocalFilesDescriptor fileMetadata, HttpHeaders headers) { // TODO max size
		long contentLength = fileMetadata.getSize();
		HttpRange range = headers.getRange().stream().findFirst().orElse(null);

		if (range != null) {

			long positions[] = new long[3];
			// start =
			positions[0] = range.getRangeStart(contentLength);
			// end =
			positions[1] = range.getRangeEnd(contentLength);
			// rangeLength =
			positions[2] = Math.min(1 * 1024 * 1024, positions[1] - positions[0] + 1);
			return positions;

		} else {
			return null;
		}
	}

	private ResourceRegion resourceRegion(LocalFilesDescriptor fileMetadata, long[] positions) {

		sharedFileSystem.getContent(fileMetadata, positions[0], positions[2], maSession);

		byte[] data = maSession.collect();
		// true size
		positions[2] = data.length;
		return new ResourceRegion(new NamedInputstreamResource(data, fileMetadata), 0, data.length);

	}

	private Resource fullResource(LocalFilesDescriptor fileMetadata) {

		sharedFileSystem.getContent(fileMetadata, 0, fileMetadata.getSize(), maSession);

		byte[] data = maSession.collect();

		return new ByteArrayResource(data);

	}

	private Page<FileItemDto> getFilesPage(int currentPage, int pageSize) {

		List<LocalFilesDescriptor> files = sharedFileSystem.listFiles();
		return findPaginated(PageRequest.of(currentPage - 1, pageSize),
				files.stream().map(this::buildFileItem).collect(Collectors.toList()));
	}

	public Page<FileItemDto> findPaginated(Pageable pageable, List<FileItemDto> descriptors) {
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		List<FileItemDto> list;

		if (descriptors.size() < startItem) {
			list = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, descriptors.size());
			list = descriptors.subList(startItem, toIndex);
		}

		Page<FileItemDto> bookPage = new PageImpl<FileItemDto>(list, PageRequest.of(currentPage, pageSize),
				descriptors.size());

		return bookPage;
	}

	private FileItemDto buildFileItem(LocalFilesDescriptor filesDescriptor) {
		return new FileItemDto() {
			{
				setOwner(filesDescriptor.getOwner().getUser());
				setPath(filesDescriptor.getDisplayPath());
				setDl(maSession.bind(filesDescriptor.getKey()));
			}
		};
	}

	public void populate(Model model) {

		if (checkSession()) {

			model.addAllAttributes(maSession.getAttributes());
			model.addAttribute("user", maSession.getUser().getUser());
			model.addAttribute("users", userService.getUsers());
			model.addAttribute("messages", tchatService.getMessages());
			model.addAttribute("home", home);

			if (!model.containsAttribute("filesPage")) {

				Page<FileItemDto> filesPage = getFilesPage(1, 5);

				model.addAttribute("filesPage", filesPage);

				int totalPages = filesPage.getTotalPages();
				if (totalPages > 0) {
					List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed()
							.collect(Collectors.toList());
					model.addAttribute("pageNumbers", pageNumbers);
				}
			}
			maSession.addAttributes(model.asMap());
		}

	}

	public boolean checkSession() {
		return maSession.getUser() != null;
	}

}
