package com.thuriercyril.bureauconfinement.sharedfilesystem;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.function.Predicate;

import javax.annotation.PreDestroy;

import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import com.hazelcast.function.ConsumerEx;
import com.hazelcast.function.FunctionEx;
import com.hazelcast.function.PredicateEx;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.Observable;
import com.hazelcast.jet.Util;
import com.hazelcast.jet.function.Observer;
import com.hazelcast.jet.impl.util.ImdgUtil;
import com.hazelcast.jet.pipeline.JournalInitialPosition;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sinks;
import com.hazelcast.jet.pipeline.Sources;
import com.hazelcast.map.EventJournalMapEvent;
import com.thuriercyril.bureauconfinement.user.CompleteUser;
import com.thuriercyril.bureauconfinement.user.UserService;

import reactor.core.publisher.Sinks.Many;

public class LocalFileSystemManager {

	private static final transient LinkOption[] emptyOptions = new LinkOption[] {};

	public static final String SHARED_FS = "shared-FS";

	@Value("${dossier-partage}")
	private String folderPath;

	@Autowired
	private JetInstance jet;

	@Autowired
	private SharedFileSystem sharedFileSystem;

	private WatchService watchService;

	private Observable<Entry<String, LocalFilesDescriptor>> observable;

	private Many<Entry<String, LocalFilesDescriptor>> sink;

	private Logger log = LoggerFactory.getLogger(getClass());

	private CompleteUser localUser;

	public void setup(CompleteUser completeuser) throws IOException {

		localUser = completeuser;

		sharedFileSystem.setAddress(localUser.getAddress());

		// Share
		Pipeline p1 = Pipeline.create();

		Predicate<EventJournalMapEvent<String, LocalFilesDescriptor>> pred = new AcceptsAll<LocalFilesDescriptor>();
		observable = jet.newObservable();

		PredicateEx<EventJournalMapEvent<String, LocalFilesDescriptor>> predEx = ImdgUtil
				.<EventJournalMapEvent<String, LocalFilesDescriptor>>wrapImdgPredicate(pred);
		FunctionEx<EventJournalMapEvent<String, LocalFilesDescriptor>, Entry<String, LocalFilesDescriptor>> fun = Util
				.<String, LocalFilesDescriptor>mapEventToEntry();

		p1.readFrom(Sources.mapJournal(SHARED_FS, JournalInitialPosition.START_FROM_OLDEST, fun, predEx))
				.withoutTimestamps().writeTo(Sinks.observable(observable));

		sink = reactor.core.publisher.Sinks.many().multicast().directAllOrNothing();
		observable.addObserver(Observer.of(new ConsumerEx<Entry<String, LocalFilesDescriptor>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -5772115353998451460L;

			@Override
			public void acceptEx(Entry<String, LocalFilesDescriptor> t) throws Exception {
				sink.tryEmitNext(t);
			}
		},

				e -> {
					e.printStackTrace();
				}, () -> {
				}));

		jet.newJob(p1);

		// Share
		watchService = FileSystems.getDefault().newWatchService();

		Path path = Paths.get(folderPath);

		if (!Files.isDirectory(path)) {
			throw new RuntimeException("incorrect monitoring folder: " + path);
		}

		log.info("START_MONITORING: {}", path);
		path.register(watchService, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY,
				StandardWatchEventKinds.ENTRY_CREATE);

		Files.walkFileTree(path, new FileVisitor<Path>() {

			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

				if (Files.exists(file, emptyOptions) && Files.isReadable(file)
						&& !Files.isDirectory(file, emptyOptions)) {

					addFileToMap(file);
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				dir.register(watchService, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY,
						StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.OVERFLOW);
				return FileVisitResult.CONTINUE;
			}
		});

	}

	@Async
	public void watch() {
		log.info("START_MONITORING");

		try {
			WatchKey key;
			while ((key = watchService.take()) != null) {
				for (WatchEvent<?> event : key.pollEvents()) {
					log.info("Event kind: {}; File affected: {}", event.kind(), event.context());
					WatchEvent.Kind<?> kind = event.kind();

					Path fileName = ((WatchEvent<Path>) event).context();
					Path base = (Path) key.watchable();
					Path complete = base.resolve(fileName);
					complete = Paths.get(URLDecoder.decode(complete.toString(), "UTF-8"));

					if (StandardWatchEventKinds.OVERFLOW == kind || StandardWatchEventKinds.ENTRY_MODIFY == kind) {
						if (Files.exists(complete.toAbsolutePath(), emptyOptions) && Files.isReadable(complete)
								&& !Files.isDirectory(complete, emptyOptions)) {

							tryReplaceFile(complete);

						}
					} else if (StandardWatchEventKinds.ENTRY_CREATE == kind) {
						if (Files.exists(complete.toAbsolutePath(), emptyOptions)) {
							if (!Files.isDirectory(complete, emptyOptions)) {
								addFileToMap(complete);
							} else {
								// watch
								complete.register(watchService, StandardWatchEventKinds.ENTRY_DELETE,
										StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_CREATE,
										StandardWatchEventKinds.OVERFLOW);
							}
						}
					} else if (StandardWatchEventKinds.ENTRY_DELETE == kind) {

						tryRemoveFile(complete);
					}
				}
				key.reset();
			}
		} catch (InterruptedException | IOException e) {
			log.warn("interrupted exception for monitoring service");
		}
	}

	private void addFileToMap(Path path) {

		String key = buildKey(path);
		LocalFilesDescriptor value = buildFileEntry(path, key);
		// preload
		Pipeline p = Pipeline.create();
		p.readFrom(sharedFileSystem.loadFile(value, 0, 0)).writeTo(Sinks.logger());
		jet.newJob(p).resume();
		jet.getHazelcastInstance().getMap(SHARED_FS).put(key, value);

	}

	private void tryRemoveFile(Path path) {

		String key = buildKey(path);
		jet.getHazelcastInstance().getMap(SHARED_FS).delete(key);
	}

	private void tryReplaceFile(Path path) {

		String key = buildKey(path);
		LocalFilesDescriptor value = buildFileEntry(path, key);
		jet.getHazelcastInstance().getMap(SHARED_FS).replace(key, value);
	}

	private String buildKey(Path path) {
		StringBuilder sb = new StringBuilder();
		sb.append(localUser.getAddress() + "#");
		sb.append(Paths.get(folderPath).relativize(path).toString());
		return sb.toString();
	}

	private LocalFilesDescriptor buildFileEntry(Path path, String key) {
		LocalFilesDescriptor descriptor = new LocalFilesDescriptor();
		CompleteUser user = (CompleteUser) jet.getHazelcastInstance().getMap(UserService.USER)
				.get(localUser.getAddress());
		descriptor.setOwner(user);
		descriptor.setPath(path.toAbsolutePath().toString());
		descriptor.setParent(path.toAbsolutePath().getParent().toString());
		descriptor.setDisplayPath(Paths.get(folderPath).relativize(path).toString().replace("\\", "/"));
		descriptor.setSize(path.toFile().length());
		descriptor.setDate(LocalDateTime.ofInstant(Instant.ofEpochMilli(path.toFile().lastModified()),
				TimeZone.getDefault().toZoneId()));
		descriptor.setKey(key);
		descriptor.setName(path.toFile().getName());
		return descriptor;
	}

	@PreDestroy
	public void stopMonitoring() {
		log.info("STOP_MONITORING");

		if (watchService != null) {
			try {
				watchService.close();
			} catch (IOException e) {
				log.error("exception while closing the monitoring service");
			}
		}
		watchService = null;
	}

	public CompleteUser getLocalUser() {
		return localUser;
	}

	public void clear() {	

		Iterator<Object> it = jet.getHazelcastInstance().getMap(SHARED_FS).keySet().iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			if (key.startsWith(localUser.getAddress())) {
				jet.getHazelcastInstance().getMap(SHARED_FS).remove(key);
			}
		}

	}

	@PreDestroy
	public void teardown() {
		clear();
		stopMonitoring();
	}

	public Publisher<String> getObservable() {
		return sink.asFlux().map(Entry::getKey).cast(String.class);
	}

	public LocalFilesDescriptor getFileDescriptor(String filepath) {
		return (LocalFilesDescriptor) jet.getHazelcastInstance().getMap(SHARED_FS).get(filepath);
	}

}
