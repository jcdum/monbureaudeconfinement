package com.thuriercyril.bureauconfinement.sharedfilesystem;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

import com.hazelcast.function.BiConsumerEx;
import com.hazelcast.function.FunctionEx;
import com.hazelcast.jet.pipeline.SourceBuilder;
import com.hazelcast.jet.pipeline.SourceBuilder.SourceBuffer;

import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

public class Streamer implements Serializable {



	/**
	 * 
	 */
	private static final long serialVersionUID = 5865511094595626722L;

	public Streamer() {
		/* */
	}

	public static FunctionEx<Path, Stream<byte[]>> streamer(long start, long rangeLength) {
		return path -> path.toAbsolutePath().toFile().exists() ? streamer(path, start, rangeLength) : null;
	}

	public static BiConsumerEx<LocalFilesDescriptor, SourceBuilder.SourceBuffer<byte[]>> source(
			LocalFilesDescriptor fileMetadata) {
		return source(Paths.get(fileMetadata.getPath()), 0, fileMetadata.getSize());
	}

	private static BiConsumerEx<LocalFilesDescriptor, SourceBuffer<byte[]>> source(Path path, int i, long size) {

		return new MyBiConsumerEx(path, i, size);
	}

	public static Stream<byte[]> streamer(Path path, long start, long rangeLength) {

		Flux<byte[]> flux = Flux.create(new Consumer<FluxSink<byte[]>>() {

			@Override
			public void accept(FluxSink<byte[]> emitter) {

				try (InputStream is = new BufferedInputStream(new FileInputStream(path.toAbsolutePath().toFile()))) {

					long toBeRead = rangeLength;

					byte[] buffer = getBuffer(rangeLength, is);

					long skipped = 0;
					while (skipped < start) {
						skipped += is.skip(start);
					}

					int read = -1;

					while ((read = is.read(buffer)) > 0) {
						byte[] data = new byte[read];
						toBeRead -= read;
						System.arraycopy(buffer, 0, data, 0, read);
						emitter.next(data);
						buffer = getBuffer(toBeRead, is);
					}
					is.close();
					emitter.complete();
				} catch (IOException e) {
					e.printStackTrace();
					emitter.error(e);
				}
			}

		});
		return flux.toStream();
	}

	public static byte[] getBuffer(long rangeLength, InputStream is) throws IOException {
		long size = Math.min(rangeLength, is.available());
		return new byte[(int) Math.min(size, 1000000L)]; // 1Mo
	}

}
