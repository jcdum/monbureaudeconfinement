package com.thuriercyril.bureauconfinement.sharedfilesystem;

import java.io.Serializable;
import java.util.UUID;

import com.hazelcast.jet.pipeline.BatchSource;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sink;
import com.hazelcast.jet.pipeline.StreamSource;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class FileRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3759298813058097135L;

	@Getter
	@Setter
	private LocalFilesDescriptor file;
	
	@Getter
	@Setter
	private Status status;
	
	@Getter
	@Setter
	private UUID token;
	
	@Getter
	@Setter
	private int port;
	
	@Getter
	@Setter
	private String host;

}
