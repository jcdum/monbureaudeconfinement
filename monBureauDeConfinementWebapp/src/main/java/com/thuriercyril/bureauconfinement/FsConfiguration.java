package com.thuriercyril.bureauconfinement;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.thuriercyril.bureauconfinement.sharedfilesystem.LocalFileSystemManager;
import com.thuriercyril.bureauconfinement.sharedfilesystem.SharedFileSystem;

@EnableAsync
@Configuration
public class FsConfiguration {
	

    @Bean(name = "threadPoolTaskExecutor")
    public Executor threadPoolTaskExecutor() {
        return new ThreadPoolTaskExecutor();
    }
    
    @Bean
    @DependsOn("sharedFileSystem")    
    public LocalFileSystemManager localFileSystemManager() {
    	return new LocalFileSystemManager();
    }
    
    @Bean
    @Qualifier("sharedFileSystem")
    public SharedFileSystem sharedFileSystem() {
    	return new SharedFileSystem();
    }

}
