package com.thuriercyril.bureauconfinement.sharedfilesystem;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import com.hazelcast.jet.pipeline.Sink;

public class SocketSink implements Sink<byte[]> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -965060863461775849L;
	private String name;
	private transient Socket socketOfClient;
	private transient OutputStream os;
	private String host;
	private int port;

	public SocketSink(String host, int port, String name) {
		this.host = host;
		this.port = port;
		this.name = name;

	}

	private void init() throws UnknownHostException, IOException {

		socketOfClient = new Socket(host, port);
		os = new BufferedOutputStream(socketOfClient.getOutputStream());

	}

	@Override
	public String name() {
		return name;
	}

	public void eat(byte[] c) {

		try {
			if (os == null) {
				init();
			}

			os.write(c);

		} catch (IOException e) {
			if(os != null) {
				try {
					os.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}

	}

	public void close() {
		if (os != null) {
			try {
			os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
