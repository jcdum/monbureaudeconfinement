package com.thuriercyril.bureauconfinement;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.slf4j.Logger;

import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.Job;
import com.hazelcast.jet.Observable;
import com.hazelcast.jet.function.Observer;
import com.hazelcast.jet.pipeline.Sink;
import com.hazelcast.jet.pipeline.Sinks;
import com.thuriercyril.bureauconfinement.user.CompleteUser;

public class MaSession implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1526582089957247515L;

	private int size = 0;

	private transient ByteArrayOutputStream os;

	private transient Job currentJob;

	private transient Logger log;

	private HashMap<String, Object> attr = new HashMap<>();

	private BidiMap techIdsMapping = new DualHashBidiMap();

	private CompleteUser user;

	public MaSession(Logger log) {
		this.log = log;
	}

	private Sink<byte[]> buildSink(JetInstance jet) {

		os = new ByteArrayOutputStream(1024 * 1024);

		Observable<byte[]> observable = jet.newObservable();
		observable.addObserver(new Observer<byte[]>() {

			@Override
			public void onNext(byte[] item) {

				try {
					size += item.length;
					os.write((byte[]) item);

					// TODO + cache
				} catch (IOException e) {
					log.error("File transfer error: ", e);
				}

			}

			@Override
			public void onComplete() {
				try {		
					os.flush();
				} catch (IOException e) {
					log.error("File transfer error: ", e);
				}
			}
		});

		return Sinks.observable(observable);

	}

	public Sink<byte[]> getSink(JetInstance jet) {
		return buildSink(jet);
	}

	public byte[] collect() {

		currentJob.join();
		byte[] data = os.toByteArray();
		os.reset();
		size = 0;
		//
		return data;
	}

	public void withJob(Job job) {
		currentJob = job;
	}

	/*
	 * Serialisation
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {

		try {

			os.flush();
			out.writeInt(size);
			byte[] bytes = os.toByteArray();
			out.write(bytes);
		} catch (Exception e) {
			log.error("Serialisation error: ", e);
		}
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		try {

			size = in.readInt();
			byte[] bytes = new byte[size];
			in.readFully(bytes, 0, size);
			os = new ByteArrayOutputStream();
			os.write(bytes);
		} catch (Exception e) {
			log.error("Deserialisation error: ", e);
		}
	}

	public CompleteUser getUser() {
		return user;
	}

	public void setUser(CompleteUser user) {
		this.user = user;
	}

	public void addAttributes(Map<String, Object> attr) {
		this.attr.putAll(attr);
	}

	public Map<String, Object> getAttributes() {
		if(attr == null)
			attr = new HashMap<>();
			
		return Collections.unmodifiableMap(attr);
	}

	public String bind(String path) {
		if(techIdsMapping == null)
			techIdsMapping = new DualHashBidiMap();
		String techId = (String) techIdsMapping.inverseBidiMap().get(path);
		if (techId == null) {
			techId = UUID.randomUUID().toString().replaceAll("-", "");
			techIdsMapping.put(techId, path);
		}
		return techId;
	}

	public Object toId(String uuid) {
		return techIdsMapping.get(uuid);
	}

}
