package com.thuriercyril.bureauconfinement.controllers.dto;

import lombok.Getter;
import lombok.Setter;

public class FileItemDto {

	@Getter
	@Setter
	private String owner;

	@Getter
	@Setter
	private String path;
	
	@Getter
	@Setter
	private String dl;



}
