package com.thuriercyril.bureauconfinement.controllers.dto;

import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class FilesListDto {

	@Getter
	@Setter
	private List<FileItemDto> files = new LinkedList<>();

}
