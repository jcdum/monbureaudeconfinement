package com.thuriercyril.bureauconfinement.tchat;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Predicate;

import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.function.ConsumerEx;
import com.hazelcast.function.FunctionEx;
import com.hazelcast.function.PredicateEx;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.Observable;
import com.hazelcast.jet.Util;
import com.hazelcast.jet.function.Observer;
import com.hazelcast.jet.impl.util.ImdgUtil;
import com.hazelcast.jet.pipeline.JournalInitialPosition;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sinks;
import com.hazelcast.jet.pipeline.Sources;
import com.hazelcast.map.EventJournalMapEvent;
import com.thuriercyril.bureauconfinement.sharedfilesystem.AcceptsAll;

import reactor.core.publisher.Sinks.Many;

@Service
public class TchatService {
	
	public static final String TCHAT = "tchat";

	private static final Object MESSAGES = "messages";
	
	@Autowired
	private transient JetInstance jet;

	private Observable<Entry<String, List<String>>> observable;

	private Many<Entry<String, List<String>>> sink;
	
	public void setup() {
		
		Pipeline p1 = Pipeline.create();

		Predicate<EventJournalMapEvent<String, List<String>>> pred = new AcceptsAll<List<String>>();
		observable = jet.newObservable();

		PredicateEx<EventJournalMapEvent<String, List<String>>> predEx = ImdgUtil
				.<EventJournalMapEvent<String, List<String>>>wrapImdgPredicate(pred);
		FunctionEx<EventJournalMapEvent<String, List<String>>, Entry<String, List<String>>> fun = Util
				.<String, List<String>>mapEventToEntry();

		p1.readFrom(Sources.mapJournal(TCHAT, JournalInitialPosition.START_FROM_OLDEST, fun, 
				predEx)).withoutTimestamps().writeTo(Sinks.observable(observable));

		sink = reactor.core.publisher.Sinks.many().multicast().directAllOrNothing();
		observable.addObserver(Observer.of(new ConsumerEx<Entry<String, List<String>>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 3340484615143019084L;

			@Override
			public void acceptEx(Entry<String, List<String>> t) throws Exception {
				sink.tryEmitNext(t);
			}
		},
				e -> {
					e.printStackTrace();
				}, () -> {}));

		jet.newJob(p1);
		
	}

	public void addMessage(String message) {		
		
		List<String> messages = getMessages();
		messages.add(message);
		jet.getHazelcastInstance().getMap(TCHAT).put(MESSAGES, messages);
		
	}

	public List<String> getMessages() {
		List<String> messages;
		Object val = jet.getHazelcastInstance().getMap(TCHAT).get(MESSAGES);
		if(val == null) {
			messages = new LinkedList<>();
		}else {
			messages = (List<String>) val;
		}
		return messages;
	}
	
	public Publisher<List<String>> getObservable() {
		return sink.asFlux().map(e -> getMessages());
	}


}
