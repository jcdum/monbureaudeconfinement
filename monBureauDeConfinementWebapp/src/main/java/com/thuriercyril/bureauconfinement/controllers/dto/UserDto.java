package com.thuriercyril.bureauconfinement.controllers.dto;

import lombok.Getter;
import lombok.Setter;

public class UserDto {
	
	@Getter
	@Setter
	private String name;

}
