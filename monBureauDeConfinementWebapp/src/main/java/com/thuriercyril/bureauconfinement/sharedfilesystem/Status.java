package com.thuriercyril.bureauconfinement.sharedfilesystem;

public enum Status {
	
	REQUEST, SOURCED, COMPLETE

}
