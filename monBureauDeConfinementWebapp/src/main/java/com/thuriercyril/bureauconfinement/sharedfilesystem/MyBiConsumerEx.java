package com.thuriercyril.bureauconfinement.sharedfilesystem;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.hazelcast.function.BiConsumerEx;
import com.hazelcast.jet.pipeline.SourceBuilder.SourceBuffer;

public class MyBiConsumerEx implements BiConsumerEx<LocalFilesDescriptor, SourceBuffer<byte[]>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2821432499345122537L;
	private int start;
	private byte[] buffer;
	private transient InputStream is;

	public MyBiConsumerEx(Path path, int start, long rangeLength) {
		try {

			is = new BufferedInputStream(new FileInputStream(path.toFile()));

			

			long skipped = 0;
			while (skipped < start) {
				skipped += is.skip(start);
			}
			
			buffer = getBuffer(is);

			this.start = start;

		} catch (IOException e) {
			e.printStackTrace();
			
		}
	}

	@Override
	public void acceptEx(LocalFilesDescriptor t, SourceBuffer<byte[]> u) throws Exception {
		
		if(is == null) {
			init(t);
		}

		int read;
		if ((read = is.read(buffer)) > 0) {
			start += read;
			byte[] data = new byte[read];
			System.arraycopy(buffer, 0, data, 0, read);
			u.add(data);
			buffer = getBuffer(is);
		}else {
			u.close();
		}

	}
	
	private void init(LocalFilesDescriptor t) {
		try {

			is = new BufferedInputStream(new FileInputStream(t.getPath()));

			

			long skipped = 0;
			while (skipped < start) {
				skipped += is.skip(start);
			}
			buffer = getBuffer(is);

		} catch (IOException e) {
			e.printStackTrace();
			
		}
		
	}

	public static byte[] getBuffer(InputStream is) throws IOException {
		return new byte[(int) Math.min(is.available(), 1000000L)]; // 1Mo
	}


}
