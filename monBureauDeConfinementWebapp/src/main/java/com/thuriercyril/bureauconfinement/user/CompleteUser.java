package com.thuriercyril.bureauconfinement.user;

import java.io.Serializable;
import java.util.HashMap;

import com.thuriercyril.bureauconfinement.sharedfilesystem.LocalFilesDescriptor;

import lombok.Getter;
import lombok.Setter;

public class CompleteUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1326329568227628407L;

	@Getter
	@Setter
	private String user;

	@Getter
	@Setter
	private String address;
	
	@Getter
	@Setter
	private HashMap<String, LocalFilesDescriptor> files;

}
