package com.thuriercyril.bureauconfinement.user;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.function.ConsumerEx;
import com.hazelcast.function.FunctionEx;
import com.hazelcast.function.PredicateEx;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.Observable;
import com.hazelcast.jet.Util;
import com.hazelcast.jet.function.Observer;
import com.hazelcast.jet.impl.util.ImdgUtil;
import com.hazelcast.jet.pipeline.JournalInitialPosition;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sinks;
import com.hazelcast.jet.pipeline.Sources;
import com.hazelcast.map.EventJournalMapEvent;
import com.thuriercyril.bureauconfinement.MaSession;
import com.thuriercyril.bureauconfinement.sharedfilesystem.AcceptsAll;
import com.thuriercyril.bureauconfinement.sharedfilesystem.LocalFilesDescriptor;

import reactor.core.publisher.Sinks.EmitResult;
import reactor.core.publisher.Sinks.Many;

@Service
public class UserService {

	public static final String USER = "user";

	@Autowired
	private transient JetInstance jet;

	@Autowired
	private transient MaSession session;

	private Observable<Entry<String, CompleteUser>> observable;

	private Many<Entry<String, CompleteUser>> sink;

	public void setup() {

		Pipeline p1 = Pipeline.create();

		Predicate<EventJournalMapEvent<String, CompleteUser>> pred = new AcceptsAll<CompleteUser>();
		observable = jet.newObservable();

		PredicateEx<EventJournalMapEvent<String, CompleteUser>> predEx = ImdgUtil
				.<EventJournalMapEvent<String, CompleteUser>>wrapImdgPredicate(pred);
		FunctionEx<EventJournalMapEvent<String, CompleteUser>, Entry<String, CompleteUser>> fun = Util
				.<String, CompleteUser>mapEventToEntry();

		p1.readFrom(Sources.mapJournal(USER, JournalInitialPosition.START_FROM_OLDEST, fun, predEx)).withoutTimestamps()
				.writeTo(Sinks.observable(observable));

		sink = reactor.core.publisher.Sinks.many().multicast().directAllOrNothing();
		observable.addObserver(Observer.of(new ConsumerEx<Entry<String, CompleteUser>>() {

			@Override
			public void acceptEx(Entry<String, CompleteUser> t) throws Exception {
				EmitResult res = sink.tryEmitNext(t);
			}
		}, e -> {
			e.printStackTrace();
		}, () -> {
		}));

		jet.newJob(p1);

	}

	public CompleteUser registerUser(String user) throws IOException {

		CompleteUser completeUser = null;
		if (user != null && !user.isEmpty()) {

			String address = jet.getCluster().getLocalMember().getAddress().getHost();
			
			System.out.println("***** "+address);

			HashMap<String, LocalFilesDescriptor> imap = new HashMap<>();

			completeUser = new CompleteUser();
			completeUser.setAddress(address);
			completeUser.setUser(user);
			completeUser.setFiles(imap);

			session.setUser(completeUser);

			jet.getHazelcastInstance().getMap(USER).put(address, completeUser);

		}else {
			throw new IllegalStateException("user empty");
		}

		return completeUser;

	}

	public void clear() {
		try {
			jet.getHazelcastInstance().getMap(USER).remove(session.getUser().getAddress());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Publisher<List<String>> getObservable() {
		return sink.asFlux().map(e -> getUsers());
	}

	public List<String> getUsers() {
		return jet.getHazelcastInstance().getMap(USER).values().stream().map(u -> ((CompleteUser) u).getUser())
				.collect(Collectors.toList());
	}

}
